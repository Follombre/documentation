#Historique des vidéos

Par défaut, PeerTube stocke l'historique des vidéos que vous avez regardées, un paramètre que vous pouvez désactiver à tout moment dans `Ma bibliothèque` > `Historique` dans le menu de gauche. Rien n'est fait avec ces données sauf pour les utilisations suivantes, et vous pouvez effacer l'historique à tout moment via le bouton situé à droite de la liste des historiques.

## Rattraper le temps perdu en regardant une vidéo

Une fois que vous avez commencé à regarder une vidéo, elle entrera dans votre historique. Mais si en plus vous quittez la vidéo avant la fin, l'historique se souviendra à quelle heure pour que vous puissiez la regarder la prochaine fois.

Cette caractéristique est représentée par une barre colorée sous la miniature vidéo.

![The video has been watched halfway, an orange bar represents it](../assets/fr-video-history-miniature-bar.png)

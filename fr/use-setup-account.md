#Paramétrer son compte

## Créer un compte PeerTube

Pour pouvoir télécharger une vidéo, vous devez avoir un compte sur un _instance_ (un serveur exécutant PeerTube). Le projet PeerTube tient à jour une liste de
instances publiques sur ce site : [instances.joinpeertube.org](https://instances.joinpeertube.org/instances) (notez que les administrateurs d'instance doivent s'ajouter eux-mêmes à cette liste manuellement, elle n'est pas générée automatiquement).

Une fois que vous avez trouvé une instance qui vous convient, cliquez sur le bouton "Créer un compte" et indiquez votre **nom d'utilisateur** (identifiant), une **adresse courriel** et un **mot de passe**.

![Page après avoir cliqué sur "Créer un compte"](../assets/fr-profile-registration.png)

## Se connecter à votre instance PeerTube

Pour vous connecter, vous devez vous rendre à l'adresse de l'instance particulière à laquelle vous vous êtes inscrit. Les instances partagent les vidéos des autres, mais
l'index des comptes de chaque instance n'est pas fédéré. Cliquez sur le bouton "Se connecter" dans le coin en haut à gauche, puis indiquez votre identifiant ou adresse courriel et mot de passe. Notez que le nom d'utilisateur et le mot de passe sont **sensibles à la casse** (PeerTube fait la distinction entre un 'a' et 'A') !

Une fois connecté, votre identifiant et votre adresse mail apparaîtront sous le nom de l'instance.

## Mettre à jour son profil

Pour mettre à jour votre profil d'utilisateur, changer votre avatar, changer votre mot de passe,... Cliquez sur les trois points verticaux près de votre nom de profil pour faire apparaître une fenêtre pop-up dans un menu, puis dans la rubrique "Mon compte". Vous avez alors quelques options :

1.  Mes paramètres
1.  Ma bibliothèque
1.  Divers

![Vue de la bibliothèque d'un utilisateur](../assets/fr-profile_library.png)

Dans l'onglet **Mes paramètres**, vous pouvez :

* changer votre avatar en envoyant, depuis votre ordinateur, une image au format .png, .jpeg ou .jpg avec une taille maxi de 100 Ko.
* voir votre quota d'upload utilisé / maximal. Ce quota est déterminé par l'instance hébergeant votre compte, et peut varier de quelques Mo à des milliers de Go,
ou même être illimité. Sur les instances qui créent des versions de qualité différente de vos vidéos après les avoir téléchargées (après une étape de transcodage vidéo),
l'espace disque occupé par toutes les versions de la vidéo est pris en compte, et pas seulement celui que vous avez téléchargé.
* modifier votre nom d'affichage, qui est différent de votre nom d'utilisateur. Votre nom d'utilisateur ne peut pas être modifié une fois créé.
* ajouter une description de l'utilisateur, qui sera affichée sur votre profil public. C'est souvent la première chose vue par les visiteurs et les utilisateurs de PeerTube, et représente votre identité, alors ne la négligez pas !
* changer votre mot de passe, en tapant deux fois votre mot de passe et en cliquant sur **_Changer le mot de passe_**.
* sur certaines instances, vous pouvez choisir comment afficher des vidéos au contenu sensible : avec flou, sans flou ou 'ne pas lister'.
* vous pouvez choisir de lire les vidéos automatiquement ou non avec une case à cocher.

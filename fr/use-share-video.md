#Partager une vidéo

Pour partager une vidéo, vous devez d'abord aller sur la page vidéo que vous voulez partager. Si vous êtes l'éditeur, vous pouvez lister toutes vos vidéos via l'option _Mes Vidéos_ du menu de gauche. Une fois sur la page vidéo, il vous suffit de cliquer sur le bouton _Partager_, et quelques options s'affichent :

![Modale présentant les options de partage](../assets/fr-video-share-modal.png)

1.  L'adresse de la vidéo, i.e.: `https://framatube.org/videos/watch/9c9de5e8-0a1e-484a-b099-e80766180a6d`. Cette adresse peut être envoyée à votre contact comme vous le souhaitez, il pourra accéder directement à la vidéo.
1.  un code d'intégration qui vous permet d'insérer un lecteur vidéo dans votre site Web.

**Astuce** : les petites icônes en fin de ligne vous permettent de copier l'URL entière en une seule fois.

# Configuration

This is still a work in progress. Please refer to the configuration instructions within the [PeerTube repository](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md#peertube-configuration).

## Environment variables

### PT_INITIAL_ROOT_PASSWORD - Initial administrator password

You can setup an initial administrator password with the environment variable `PT_INITIAL_ROOT_PASSWORD`, it must be 6 characters or more.

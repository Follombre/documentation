I know this might seem obvious, especially if you come here from [joinpeertube.org](http://joinpeertube.org/) - but it's good to review things together before installing, isn't it?

## What is PeerTube? <a class="toc" id="toc-what-is-peertube" href="#toc-what-is-peertube"></a>

PeerTube is a video hosting software project. It allows you to watch and share videos
with others using your own server, be it a simple computer board at home or a blade in a datacenter. You don't need to host thousands of videos to make your instance interesting for daily use. In a federated fashion, it will talk _with other servers of your choice_ to aggregate references to their videos without really hosting them locally. What's more, we also aim to play nice with others: venerable MediaGoblin instances that have filled the gap of video hosting for so many years will eventually get federated with PeerTube instances, making the whole federation even bigger.

PeerTube also relies on existing open technologies like BitTorrent as a streaming transport layer, making people watching your video able to share it while they watch it (and even beyond that provided they have a WebTorrent-compatible torrent daemon). This has the advantage of lowering the load on the server, making it _slightly_ more scalable.

PeerTube is close to other federated communities, traditionally more oriented towards purely social interaction in a Twitter-like fashion. We also interact with them thanks to ActivityPub, rendering videos commentable all over the fediverse! Plus, you can follow them thanks to ActivityStreams and RSS/Atom/Json-feed feeds.

Last but not least, PeerTube is an open platform that aggregates these wonderful technologies. There are many more to come, but the most wonderful is what you do with it: sharing video, music, finally unleashed.

## Why do we need it? <a class="toc" id="toc-why-do-we-need-it" href="#toc-why-do-we-need-it"></a>

Plenty of reasons. Ask yourself why you are here.

It might be because you want to find a sense of community-driven content creatoin again. It's not about creating closed communities - it's about
knowing that the content you see comes to you in a natural way, rather than being driven by an obscure and ominous algorithm, designed by
advertisers that drive content based on how much they can make you buy with it.

It might be because you got ripped off on some other video hosting  (getting bullied by ContentID and losing days of revenue).

It might be because you want to use open source, libre technologies. We're not especially fond of the GNU philosophy, but we understand that 
the GNU conception of libre software strikes a chord, compared to ordinary "open source" software. PeerTube is licensed under the [GNU Affero General Public License](https://github.com/Chocobozzz/PeerTube/blob/develop/LICENSE).

It might be because you want to use the best video formats available and contribute to making them widespread - something that we are actively working on.

It might be because you don't want to be tracked.

## How ? <a class="toc" id="toc-how" href="#toc-how"></a>

Because we could. And now it's your turn.

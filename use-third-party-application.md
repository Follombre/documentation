# Third party applications

## Browser addons

 * [Peertubeify](https://gitlab.com/Ealhad/peertubeify): browser addon to show if a YouTube video exists on PeerTube (by default, it uses [peertube.social](https://peertube.social) to perform the search, but you can change it easily). Available for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/peertubeify/) and on the [Chrome web store](https://chrome.google.com/webstore/detail/peertubeify/gegmikcnabpilimgelhabaledkcikdab)


## Android applications

 * [Thorium](https://github.com/sschueller/peertube-android): an Android PeerTube Client 

## Kodi addons

 * KODI plugin that allows to stream from instances listed publicly, or custom ones you set: https://framagit.org/StCyr/plugin.video.peertube | it only downloads via libtorrent that doesn't support WebTorrent.

## CLI scripts

 * Toot new videos on Mastodon automatically (Python): https://github.com/PhieF/MiscConfig/blob/master/Peertube/peertubetomasto.py
 * Prismedia: scripting video uploads to PeerTube and YouTube at the same time (Python): https://git.lecygnenoir.info/LecygneNoir/prismedia
 * **[FRENCH]** Seed automatically with some bash-fu and WebTorrent Desktop: https://linuxfr.org/users/yolo42/journaux/repliquer-ses-videos-peertube-premiers-pas
